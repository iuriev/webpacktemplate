import { assert, expect } from 'chai';
import sinon from "sinon"
import Layout from '../Layout.jsx';
import React from "react";
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import mochaSnapshots from 'mocha-snapshots';
configure({ adapter: new Adapter() });
mochaSnapshots.setup({ sanitizeClassNames: false });
import {JSDOM} from "jsdom";
// const doc = new JSDOM()
// global.document = doc;
// global.window = doc.defaultView;

describe('<Layout />', () => {
    it('should match snapshot', () => {
        const wrapper = shallow(<Layout />)

        // You can match Enzyme wrappers
        expect(wrapper).to.matchSnapshot();

        // Strings
        expect('you can match strings').to.matchSnapshot();

        // Numbers
        expect(123).to.matchSnapshot();

        // Or any object
        expect({ a: 1, b: { c: 1 } }).to.matchSnapshot();

    });
    it('mount', () => {
        const layout = mount(<Layout />)
        sinon.spy(layout.changeLocale);


        layout.find(".className").simulate("click")


        layout.find(".aim").simulate("click")
        sinon.assert.calledWith(layout.changeLocale, "ru");
        assert.isTrue()
        assert.strictEqual(layout.state.locale.currentLocale, "ru")

    });

});