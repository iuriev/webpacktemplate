import React from 'react';
import Header from "../header/Header.jsx";

class Layout extends React.Component {

    render() {
        return (
            <div className="wrapper">
                <div className="header-wrapper">
                    <Header/>
                </div>
                <main className="main">
                </main>
            </div>
        )
    }
}

export default Layout;
